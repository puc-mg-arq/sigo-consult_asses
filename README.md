# SIGO Consultoria e Assessoria

Projeto Consultoria e Assessoria do sistema SIGO.

## Comunicacao

### Empresa - Registrar 

Registrar uma empresa.

#### Request

* POST /sigo-consult_asses/empresa HTTP/1.1
* Host: localhost:5002

ex.:

```
curl -vvv --location --request POST 'http://localhost:5002/sigo-consult_asses/empresa' --header 'Content-Type: application/json' --data-raw '{
    "nome":"[EMPRESA_CNPJ]",
    "cnpj": "[EMPRESA_CNPJ]",
    "telefone":"[EMPRESA_TELEFONE]"
}'
```

#### Response

* Status Code: 200 - OK
* Body:


```
{
    "id": [EMPRESA_ID],
    "cnpj": "[EMPRESA_CNPJ]",
    "nome": "[EMPRESA_NOME]",
    "telefone": "[EMPRESA_TELEFONE]"
}
```

### Empresa - Pesquisar 

Pesquisar por uma empresa.


#### Request

* GET /sigo-consult_asses/empresa/?nome=[EMPRESA_NOME]&cnpj=[EMPRESA_CNPJ] HTTP/1.1
* Host: localhost:5002

ex.:

```
curl -vvv --location --request GET 'http://localhost:5002/sigo-consult_asses/empresa/?nome=[EMPRESA_NOME]&cnpj=[EMPRESA_CNPJ]' --header 'Content-Type: application/json' --data-raw '{
    "nome":"[EMPRESA_NOME]",
    "cnpj": "[EMPRESA_CNPJ]",
    "telefone":"[EMPRESA_TELEFONE]"
}'
```

#### Response

* Status Code: 200 - OK
* Body:

```

[
  {
    "id":[EMPRESA_ID],
    "cnpj":"[EMPRESA_CNPJ]",
    "nome":"[EMPRESA_NOME]",
    "telefone":"[EMPRESA_TELEFONE]"
  }
]
```

### Empresa - Buscar por ID

Buscar uma empresa por ID.

#### Request

* GET /sigo-consult_asses/empresa/[EMPRESA_ID] HTTP/1.1
* Host: localhost:5002

ex.:

```
curl -vvv --location --request GET 'http://localhost:5002/sigo-consult_asses/empresa/[EMPRESA_ID]' --header 'Content-Type: application/json' --data-raw '{
    "nome":"[EMPRESA_NOME]",
    "cnpj": "[EMPRESA_CNPJ]",
    "telefone":"[EMPRESA_TELEFONE]"
}'
```

#### Response

* Status Code: 200 - OK
* Body:

```
  {
    "id":[EMPRESA_ID],
    "cnpj":"[EMPRESA_CNPJ]",
    "nome":"[EMPRESA_NOME]",
    "telefone":"[EMPRESA_TELEFONE]"
  }
```

### Empresa - Remover

Remover uma empresa por ID.

#### Request

* DELETE /sigo-consult_asses/empresa/[EMPRESA_ID] HTTP/1.1
* Host: localhost:5002

ex.:

```
curl -vvv --location --request DELETE 'http://localhost:5002/sigo-consult_asses/empresa/[EMPRESA_ID]' 
```

#### Response

* Status Code: 200 - OK
