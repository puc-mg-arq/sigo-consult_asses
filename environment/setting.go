package environment

import (
	"encoding/json"
	"time"
)

type setting struct {
	Server struct {
		Context string `envconfig:"SERVER_CONTEXT" default:"sigo-consult_asses"`
		Port    string `envconfig:"PORT" default:"5002" required:"true" ignored:"false"`
	}

	Connection struct {
		MaxIdleConnections int `envconfig:"CONNECTION_MAX_IDLE_CONNECTIONS" default:"100"`
		RequestTimeout     int `envconfig:"CONNECTION_REQUEST_TIMEOUT" default:"2000"`
	}

	PostgresDB struct {
		Host           string        `envconfig:"SIGO_POSTGRES_HOST" default:"192.168.0.100"`
		User           string        `envconfig:"SIGO_POSTGRES_USER" default:"sigo"`
		Password       string        `envconfig:"SIGO_POSTGRES_PASSWORD" default:"sigo1234"`
		DBName         string        `envconfig:"SIGO_POSTGRES_DB" default:"sigo"`
		Port           string        `envconfig:"SIGO_POSTGRES_PORT" default:"5432"`
		DBType         string        `envconfig:"SIGO_POSTGRES_DBTYPE" default:"postgres"`
		MinPoolSize    uint64        `envconfig:"SIGO_POSTGRES_MIN_POOL_SIZE" default:"20"`
		ConnectTimeout time.Duration `envconfig:"SIGO_POSTGRES_CONNECT_TIMEOUT" default:"3s"`
	}

}

var Setting setting

func (set *setting) GetJson() []byte {

	data, err := json.Marshal(set)
	if err != nil {
		data, _ = json.Marshal(setting{})
	}

	return data
}
