package repository

import (
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/util"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"time"
)

type PostgresDB struct {
	DB *gorm.DB
}

func GetDBType() string {
	return "postgres"
}

func GetPostgresConnectionString(host, port, user, name, password string) string {

	dataBase := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		host,
		port,
		user,
		name,
		password)
	return dataBase
}

func NewDB(host, port, user, name, password string,
	logging util.Logging) *PostgresDB {

	conString := GetPostgresConnectionString(host, port, user, name, password)

	logg := logging.LoggerMessageDefault("host", host, "port", port, "user", user, "name", name)
	logg.LoggerMessage("conectando no postgres db", "conString", conString)

	DB, err := connecting(logging, conString)
	if err != nil {
		logging.LoggerMessage("error ao inicializar postgres db", "error", err.Error())
		panic(err)
	}

	postgresDB := PostgresDB{
		DB: DB,
	}

	return &postgresDB
}

func connecting(logging util.Logging, conString string) (*gorm.DB, error) {

	tryConnect := 1

	for {
		logging.LoggerMessage("tentativa inicializar postgres db", "tentativa", tryConnect)
		DB, err := gorm.Open(GetDBType(), conString)
		if err != nil && tryConnect != 3 {

			tryConnect++
			if tryConnect > 3 {
				logging.LoggerMessage("error ao inicializar postgres db", "tentativas para inicializar", tryConnect)
				return nil, err
			}

			time.Sleep(3 * time.Second)
			continue
		}

		return DB, err
	}
}
