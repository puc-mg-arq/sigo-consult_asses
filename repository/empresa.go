package repository

import (
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/model"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/util"
	"fmt"
	"strconv"
)

const (
	TableNameEmpresa = "sigo_consult_asses.empresa"
)

type Empresa struct {
	Logging    util.Logging
	PostgresBD *PostgresDB
}

func (repository *Empresa) Registrar(emp *model.Empresa) error {

	if emp.ID == 0 {
		if err := repository.PostgresBD.DB.Table(TableNameEmpresa).Create(emp).Error; err != nil {
			return err
		}
		return nil
	}

	if err := repository.PostgresBD.DB.Table(TableNameEmpresa).Model(emp).Update(emp).Error; err != nil {
		return err
	}

	return nil
}

func (repository *Empresa) Remover(emp *model.Empresa) error {

	if err := repository.PostgresBD.DB.Table(TableNameEmpresa).Delete(emp).Error; err != nil {
		return err
	}

	return nil
}

func (repository *Empresa) GetByID(ID string) (model.Empresa, error) {

	var emp model.Empresa

	atoi, err := strconv.ParseInt(ID, 10, 0)
	if err != nil {
		return emp, err
	}

	//sql := fmt.Sprintf("select * from %s where id = ? order by id limit 1", TableNameEmpresa)
	//result := repository.PostgresBD.DB.Raw(sql, atoi).Scan(&emp)
	result := repository.PostgresBD.DB.Table(TableNameEmpresa).First(&emp, atoi)

	return emp, result.Error
}

func (repository *Empresa) GetBy(cnpj, nome string) ([]model.Empresa, error) {

	var emps []model.Empresa
	sql := fmt.Sprintf("select * from %s where 1 = 1 ", TableNameEmpresa)
	if len(cnpj) > 0 {
		sql += fmt.Sprintf("and cnpj = '%s' ", cnpj)
	}

	if len(nome) > 0 {
		sql += fmt.Sprintf("and nome ilike ('%s') ", "%"+nome+"%")
	}

	sql += " order by nome"

	result := repository.PostgresBD.DB.Raw(sql).Scan(&emps)

	return emps, result.Error
}
