module bitbucket.org/puc-mg-arq/sigo-consult_asses

go 1.15

require (
	github.com/go-chi/chi v1.5.2
	github.com/jinzhu/gorm v1.9.16
	github.com/kelseyhightower/envconfig v1.4.0
)
