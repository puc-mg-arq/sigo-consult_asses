package service

import (
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/model"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/repository"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/util"
	"errors"
)

type Empresa struct {
	Logging    util.Logging
	EmpresaRepository *repository.Empresa
}

func (service *Empresa) Registrar (empresa model.Empresa) (model.Empresa, error) {

	if err := service.EmpresaRepository.Registrar(&empresa); err != nil {
		service.Logging.LoggerMessage("falha ao registrar empresa", "cnpj", empresa.Cnpj)
		return empresa, err
	}

	return empresa, nil
}

func (service *Empresa) ValidarRegistrar (empresa model.Empresa) error {

	logging := service.Logging.LoggerMessageDefault("cnpj", empresa.Cnpj, "nome", empresa.Nome,
		"telefone", empresa.Telefone, "id", empresa.ID)
	if len(empresa.Cnpj) == 0 || len(empresa.Nome) == 0 || len(empresa.Telefone) == 0 {
		logging.LoggerMessage("validar empresa", "msg", "faltando dados")
		return errors.New("dados incompletos")
	}

	logging.LoggerMessage("validar empresa", "msg", "dados okay")
	return nil
}

func (service *Empresa) GetByID (ID string) (model.Empresa, error) {

	logging := service.Logging.LoggerMessageDefault( "id", ID)
	empresa, err := service.EmpresaRepository.GetByID(ID)
	if err != nil {
		logging.LoggerMessage("falha ao buscar empresa por id", "error", err.Error())
		return empresa, err
	}

	logging.LoggerMessage("sucesso ao buscar empresa por id", "cnpj", empresa.Cnpj, "nome", empresa.Nome,
		"telefone", empresa.Telefone, "id", empresa.ID)

	return empresa, nil
}

func (service *Empresa) GetBy(cnpj, nome string) ([]model.Empresa, error) {

	empresas, err := service.EmpresaRepository.GetBy(cnpj, nome)

	return empresas, err
}

func (service *Empresa) Remover (ID string) error {

	empresa, err := service.GetByID(ID)
	if err != nil {
		service.Logging.LoggerMessage("falha ao remover empresa por id", "ID", ID, "error", err.Error())
		return err
	}

	if err := service.EmpresaRepository.Remover(&empresa); err != nil {
		service.Logging.LoggerMessage("falha ao remover empresa", "cnpj", empresa.Cnpj, "ID", empresa.ID)
		return err
	}

	return nil
}