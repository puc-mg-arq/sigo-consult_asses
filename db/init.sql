create schema sigo_consult_asses

    create table empresa
    (
        id       serial       not null
            constraint sigo_empresa_pkey
                primary key,
        nome     varchar(255) not null,
        telefone varchar(255) not null,
        cnpj     varchar(255) not null
    )

create
unique index sigo_consult_asses_id_uindex
    on empresa (id);

alter table sigo_consult_asses.empresa owner to sigo;

INSERT INTO sigo_consult_asses.empresa (nome, telefone, cnpj)
VALUES ('Empresa', '21912345678', '45.845.308/0001-76');

