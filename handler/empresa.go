package handler

import (
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/model"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/service"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/util"
	"encoding/json"
	"github.com/go-chi/chi"
	"io/ioutil"
	"net/http"
)

type Empresa struct {
	Logging        util.Logging
	EmpresaService *service.Empresa
}

func (handler *Empresa) Registrar(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		handler.Logging.LoggerMessage("error ao decodificar corpo do request", "error", err.Error())
		marshal, _ := json.Marshal(map[string]string{"error": err.Error()})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)
		return
	}

	var empresa model.Empresa
	json.Unmarshal(body, &empresa)

	if err := handler.EmpresaService.ValidarRegistrar(empresa); err != nil {
		marshal, _ := json.Marshal(map[string]string{"error": err.Error()})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)
		return
	}

	empresa, err = handler.EmpresaService.Registrar(empresa)
	if err != nil {
		handler.Logging.LoggerMessage("error ao registrar empresa", "error", err.Error())
		marshal, _ := json.Marshal(map[string]string{"error": err.Error()})
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(marshal)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	marshal, _ := json.Marshal(empresa)
	w.Write(marshal)
}

func (handler *Empresa) GetBy(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	cnpj := r.URL.Query().Get("cnpj")
	nome := r.URL.Query().Get("nome")
	empresas, err := handler.EmpresaService.GetBy(cnpj, nome)
	if err != nil {
		handler.Logging.LoggerMessage("error ao pesquisar empresa", "error", err.Error())
		marshal, _ := json.Marshal(map[string]string{"error": err.Error()})
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(marshal)
		return
	}

	w.WriteHeader(http.StatusOK)
	marshal, _ := json.Marshal(empresas)
	w.Write(marshal)
}

func (handler *Empresa) GetByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	ID := chi.URLParam(r, "ID")
	empresa, err := handler.EmpresaService.GetByID(ID)
	if err != nil {
		marshal, _ := json.Marshal(map[string]string{"error": err.Error()})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)
		return
	}

	if empresa.ID == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	marshal, _ := json.Marshal(empresa)
	handler.Logging.LoggerMessage("empresa encontrada", "body", string(marshal))
	w.WriteHeader(http.StatusOK)
	w.Write(marshal)
}

func (handler *Empresa) Remover(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	ID := chi.URLParam(r, "ID")
	err := handler.EmpresaService.Remover(ID)
	if err != nil {
		marshal, _ := json.Marshal(map[string]string{"error": err.Error()})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)
		return
	}

	handler.Logging.LoggerMessage("empresa removida", "ID", ID)
	w.WriteHeader(http.StatusOK)
}

