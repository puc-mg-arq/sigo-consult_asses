package model

type Empresa struct {
	ID       int64  `gorm:"id" json:"id"`
	Cnpj     string `gorm:"snpj" json:"cnpj"`
	Nome     string `gorm:"nome" json:"nome"`
	Telefone string `gorm:"telefone" json:"telefone"`
}
