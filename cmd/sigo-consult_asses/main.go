package main

import (
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/environment"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/handler"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/repository"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/service"
	"bitbucket.org/puc-mg-arq/sigo-consult_asses/util"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/kelseyhightower/envconfig"
	"log"
	"net/http"
	"os"
	"time"
)

var logging util.Logging

func init() {

	logger := log.New(os.Stdout, "", log.Flags())
	logging = util.Logging{
		Logger: logger,
	}

	err := envconfig.Process("setting", &environment.Setting)
	if err != nil {
		panic(err.Error())
	}

	logging.LoggerMessage("starting environment setting", "environment.Setting", string(environment.Setting.GetJson()))

}

func main() {

	postgresDB := repository.NewDB(environment.Setting.PostgresDB.Host, environment.Setting.PostgresDB.Port,
		environment.Setting.PostgresDB.User, environment.Setting.PostgresDB.DBName,
		environment.Setting.PostgresDB.Password, logging)

	empresaRepository := repository.Empresa{
		Logging:    logging,
		PostgresBD: postgresDB,
	}

	empresaService := service.Empresa{
		Logging:    logging,
		EmpresaRepository: &empresaRepository,
	}

	empresaHandler := handler.Empresa{
		Logging: logging,
		EmpresaService: &empresaService,
	}

	router := chi.NewRouter()
	context := environment.Setting.Server.Context

	router.Get(fmt.Sprintf("/%s/health", context),func (w http.ResponseWriter, r *http.Request) {

		w.Header().Add("Content-Type", "application/json")

		logging.LoggerMessage("health check")

		w.WriteHeader(http.StatusOK)
		marshal, _ := json.Marshal(map[string]interface{}{"status": "up"})
		w.Write(marshal)

	})

	router.Route(fmt.Sprintf("/%s/empresa", context), func(router chi.Router) {
		router.Post(fmt.Sprintf("/"), empresaHandler.Registrar)
		router.Get(fmt.Sprintf("/"), empresaHandler.GetBy)
		router.Get(fmt.Sprintf("/{ID:[a-zA-Z0-9]}"), empresaHandler.GetByID)
		router.Delete(fmt.Sprintf("/{ID:[a-zA-Z0-9]}"), empresaHandler.Remover)
	})

	serverHttp := &http.Server{
		Addr:           fmt.Sprintf(":%s", environment.Setting.Server.Port),
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	logging.LoggerMessage("server starting", "port", serverHttp.Addr, "context", context)
	if err := serverHttp.ListenAndServe(); err != nil {
		logging.LoggerMessage("listen and serve", "err", err.Error())
		panic(err.Error())
	}

}
