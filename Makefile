#makefile

APPLICATION_NAME := sigo-consult_asses
PORT := 5000

build:
	docker build -t $(APPLICATION_NAME):latest .

build-run:	build
	docker run -p $(PORT):$(PORT) -t $(APPLICATION_NAME):latest
#	docker run -e "REDIS_ADDR=value" -e "REDIS_PASSWORD=value" -p $(PORT):$(PORT) -t $(APPLICATION_NAME):latest

run:
	docker run -p $(PORT):$(PORT) -t $(APPLICATION_NAME):latest
