package util

import (
	"encoding/json"
	"log"
	"time"
)

type Logging struct {
	Logger         *log.Logger
	mappingMessage map[string]interface{}
}

func (l Logging) LoggerMessage(message string, v ...interface{}) {

	l.mappingMessage = interfaceArrayToMap(v)
	l.mappingMessage["message"] = message
	l.mappingMessage["date"] = time.Now().Format(time.RFC3339Nano)

	marshal, _ := json.Marshal(l.mappingMessage)
	l.Logger.Println(string(marshal))

}

func (l Logging) LoggerMessageDefault(v ...interface{}) Logging {

	l.mappingMessage = interfaceArrayToMap(v)
	return l
}

func interfaceArrayToMap(v ...interface{}) map[string]interface{} {

	mapping := make(map[string]interface{})
	if len(v) > 0 {
		m := v[0].([]interface{})
		//fmt.Println(fmt.Sprintf("m: %v", m))
		for i := 0; i < len(m); i++ {
			if i%2 == 1 {
				mapping[m[i-1].(string)] = m[i]
			}
		}
	}

	return mapping
}
