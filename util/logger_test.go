package util

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"log"
)

func TestLogger(t *testing.T)  {

	logTest("testando", "num1", "1", "num2", 2, "num3", 3)

}

func logTest(message string, v ... interface{}) {

	mapping := map[string]interface{}{
		"message": message,
	}
	for i := 0; i < len(v); i++ {
		fmt.Println(fmt.Sprintf("k: %v - v: %v", i, v[i]))
		if i % 2 == 1 {
			mapping[v[i-1].(string)] = v[i]
		}
	}

	marshal, _ := json.Marshal(mapping)
	logger := log.New(os.Stdout, "", log.Flags())
	logger.Println(string(marshal))

}
